# court-counter
Court counter gives a user the ability to keep track of the score of two different teams playing a game of your choice.


### Objectives
* Learn Adding button code to your app
* Learn Updating views
* Properly scoping variables
* Learn Finding views by their ID

### Build Instructions
This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

### screenshots
![please find images under app-screenshots directory](app-screenshots/screen-1.png "screen one")
![please find images under app-screenshots directory](app-screenshots/screen-2.png "screen two")
